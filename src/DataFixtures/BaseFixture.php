<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Faker\Factory;
use Faker\Generator;

abstract class BaseFixture extends Fixture
{
    /** @var ObjectManager */
    private $manager;

    /** @var Generator */
    protected $faker;

    protected static $referencesIndex = [];

    abstract protected function loadData(ObjectManager $manager);

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $this->faker = Factory::create();
        $this->loadData($manager);
        $manager->flush();
    }

    public function createMultiple(string $className, int $count, callable $factory)
    {
        self::$referencesIndex[$className] = [];
        for ($i = 0; $i < $count; $i++) {
            $entity = new $className();
            $factory($entity, $i);
            $this->manager->persist($entity);
            $this->addReference($className . '_' . $i, $entity);
            self::$referencesIndex[$className][] = $className . '_' . $i;
        }
    }

    /**
     * @throws Exception
     */
    public function getRandomReference($className): object
    {
        if (!isset(self::$referencesIndex[$className])) {
            throw new Exception(sprintf('can\'t find reference for class %s', $className));
        }
        return $this->getReference($this->faker->randomElement(self::$referencesIndex[$className]));
    }
}
