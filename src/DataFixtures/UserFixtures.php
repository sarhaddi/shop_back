<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends BaseFixture
{
    /**
     * @var UserPasswordHasherInterface
     */
    private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function loadData(ObjectManager $manager)
    {
        $this->createMultiple(User::class, 5, function (User $user, int $i) {
            $user->setEmail($this->faker->email);
            $user->setAddress($this->faker->address);
            $user->setBirthDate($this->faker->dateTime);
            $user->setFullName($this->faker->name);
            $user->setPassword($this->passwordHasher->hashPassword($user, 'QWERTqwert'));
            $user->setPostalCode($this->faker->numerify('##########'));
        });
    }
}
