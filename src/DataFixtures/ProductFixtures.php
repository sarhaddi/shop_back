<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Product;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ProductFixtures extends BaseFixture implements DependentFixtureInterface
{
    public function loadData(ObjectManager $manager)
    {
        $this->createMultiple(Product::class, 50, function (Product $product, int $i) {
            $product->setName($this->faker->words(3, true));
            if (!file_exists(__DIR__ . '/../../public/images/' . $i . '.jpg')) {
                $pic = $this->faker->imageUrl(480, 480, 'fashion');
                file_put_contents(__DIR__ . '/../../public/images/' . $i . '.jpg', file_get_contents($pic));
            }
            $product->setPic($i . '.jpg');
            $product->setCategory($this->getRandomReference(Category::class));
            $product->setPrice($this->faker->numberBetween(10, 10000));
            $product->setView($this->faker->numberBetween(10, 500));
            $product->setCreatedAt($this->faker->dateTimeBetween('-5 month'));
        });
    }

    public function getDependencies()
    {
        return [
            CategoryFixtures::class
        ];
    }
}
