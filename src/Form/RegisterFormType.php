<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class RegisterFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('password', null, [
                'mapped' => false,
                'constraints' => [
                    new NotBlank(),
                    new Length(null, 10),
                    new Regex(
                        "/^(([^A-Z]+[A-Z]+[^A-Z]*)|([^A-Z]*[A-Z]+[^A-Z]+)|([A-Z]*))+$/s",
                        "{{ label }} must have uppercase character"
                    )
                ]
            ])
            ->add('fullName')
            ->add('birthDate', DateType::class, [
                'widget' => 'single_text'
            ])
            ->add('address')
            ->add('postalCode');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
