<?php

namespace App\Controller;

use App\Entity\Cart;
use App\Entity\CartProduct;
use App\Entity\User;
use App\Form\CartType;
use App\Repository\CartRepository;
use App\Service\ApiFormHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

/**
 * @Route("/api/cart")
 */
class CartController extends AbstractController
{
    /**
     * @Route("", name="cartList", methods={"GET"})
     */
    public function list(EntityManagerInterface $em): Response
    {
        /** @var CartRepository $repository */
        $repository = $em->getRepository(Cart::class);
        return $this->json($repository->findByUser($this->getUser()), 200, [], [AbstractNormalizer::IGNORED_ATTRIBUTES => ['cartProducts']]);
    }

    /**
     * @Route("/{id<\d+>}", name="cartList", methods={"GET"})
     */
    public function show(Cart $cart, EntityManagerInterface $em): Response
    {
        return $this->json($cart);
    }

    /**
     * @Route("", name="cartAdd", methods={"POST"})
     */
    public function add(Request $request, ApiFormHelper $formHelper, EntityManagerInterface $em): Response
    {
        $form = $formHelper->createForm($request, CartType::class);
        if ($form->isSubmitted() and $form->isValid()) {
            /** @var Cart $cart */
            $cart = $form->getData();
            /** @var User $user */
            $user = $this->getUser();
            $cart->setUser($user);
            $em->persist($cart);
            $em->flush();
        } else {
            return $this->json($formHelper->formErrors($form), Response::HTTP_BAD_REQUEST);
        }
        /** @var CartRepository $repository */
        $repository = $em->getRepository(Cart::class);
        return $this->json($repository->findByUser($this->getUser()), 200, [], [AbstractNormalizer::IGNORED_ATTRIBUTES => ['cartProducts']]);
    }
}
