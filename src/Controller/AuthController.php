<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegisterFormType;
use App\Service\ApiFormHelper;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/auth")
 */
class AuthController extends AbstractController
{
    /**
     * @Route("/register", name="authRegister", methods={"POST"})
     */
    public function Register(Request $request, ApiFormHelper $formHelper, EntityManagerInterface $em, JWTTokenManagerInterface $JWTManager, UserPasswordHasherInterface $passwordHasher): Response
    {
        $form = $formHelper->createForm($request, RegisterFormType::class);
        if ($form->isSubmitted() and $form->isValid()) {
            $password = $form->get('password')->getData();
            /** @var User $user */
            $user = $form->getData();
            $user->setPassword($passwordHasher->hashPassword($user, $password));
            $em->persist($user);
            $em->flush();
            return $this->json(['token' => $JWTManager->create($user)]);
        }
        return $this->json($formHelper->formErrors($form), Response::HTTP_BAD_REQUEST);
    }
}
