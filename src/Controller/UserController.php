<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Service\ApiFormHelper;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("", name="userProfile", methods={"GET"})
     */
    public function profile(): Response
    {
        return $this->json($this->getUser());
    }

    /**
     * @Route("", name="userUpdate", methods={"PUT"})
     */
    public function update(Request $request, ApiFormHelper $formHelper, UserPasswordHasherInterface $passwordHasher, EntityManagerInterface $em): Response
    {
        $user = $this->getUser();
        $form = $formHelper->createForm($request, UserType::class, $user);
        if ($form->isSubmitted() and $form->isValid()) {
            $password = $form->get('password')->getData();
            /** @var User $user */
            $user = $form->getData();
            if ($password) {
                $user->setPassword($passwordHasher->hashPassword($user, $password));
            }
            $em->persist($user);
            $em->flush();
        } else {
            return $this->json($formHelper->formErrors($form), Response::HTTP_BAD_REQUEST);
        }
        return $this->json($user);
    }
}
