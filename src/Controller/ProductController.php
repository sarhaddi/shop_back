<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/product")
 */
class ProductController extends AbstractController
{
    /**
     * @Route("", name="productList", methods={"GET"})
     */
    public function list(EntityManagerInterface $em): Response
    {
        /** @var ProductRepository $repository */
        $repository = $em->getRepository(Product::class);
        return $this->json($repository->autoFind());
    }

    /**
     * @Route("/{id<\d+>}", name="productShow", methods={"GET"})
     */
    public function show(Product $product): Response
    {
        return $this->json($product);
    }
}
