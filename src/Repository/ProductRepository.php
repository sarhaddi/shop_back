<?php

namespace App\Repository;

use App\Entity\Product;
use App\Service\QueryHandler;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    /**
     * @var QueryHandler
     */
    private $queryHandler;

    public function __construct(ManagerRegistry $registry, QueryHandler $queryHandler)
    {
        parent::__construct($registry, Product::class);
        $this->queryHandler = $queryHandler;
    }

    public function autoFind()
    {
        $query = $this->createQueryBuilder('p');
        $this->queryHandler->orderByRequest($query, ['view' => 'p.view', 'createdAt' => 'p.createdAt', 'price' => 'p.price']);
        $this->queryHandler->paginationByRequest($query);
        $this->queryHandler->filterByRequest($query, 'p.name');
        return $query->getQuery()->getResult();
    }
}
