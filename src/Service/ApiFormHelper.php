<?php


namespace App\Service;


use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class ApiFormHelper
{
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    public function __construct(FormFactoryInterface $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    public function createForm(Request $request, $formClass, $data = null, array $options = ['csrf_protection' => false]): FormInterface
    {
        if (!isset($options['csrf_protection'])) {
            $options['csrf_protection'] = false;
        }
        $form = $this->formFactory->create($formClass, $data, $options);
        $form->submit($request->toArray());
        return $form;
    }

    public function formErrors(FormInterface $form): array
    {
        $errors = [];
        foreach ($form->getErrors(true, false) as $errorIterator) {
            if ($errorIterator instanceof FormErrorIterator) {
                $errors[$errorIterator->getForm()->getName()] = [];
                foreach ($errorIterator as $error) {
                    $errors[$errorIterator->getForm()->getName()][] = $error->getMessage();
                }
            } elseif ($errorIterator instanceof FormError) {
                $errors[] = $errorIterator->getMessage();
            }
        }
        return $errors;
    }
}