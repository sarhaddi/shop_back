<?php


namespace App\Service;


use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class QueryHandler
{
    /**
     * @var Request
     */
    private $request;

    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    public function orderByRequest(QueryBuilder $query, array $columns): void
    {
        if ($order = $this->request->get('order')) {
            $type = substr($order, 0, 1) == '-' ? 'DESC' : 'ASC';
            $order = substr($order, 0, 1) == '-' ? substr($order, 1) : $order;
            if (isset($columns[$order])) {
                $query->orderBy($columns[$order], $type);
            }
        }
    }

    public function paginationByRequest(QueryBuilder $query, $perPage = 10, $maxPerPage = 50): void
    {
        if ($requestPerPage = $this->request->get('perPage') and $requestPerPage < $maxPerPage)
            $perPage = $requestPerPage;
        $offset = (($this->request->get('page') ?? 1) - 1) * $perPage;
        $query->setMaxResults($perPage)->setFirstResult($offset);
    }

    public function filterByRequest(QueryBuilder $query, $column, $like = true)
    {
        if ($filter = $this->request->get('search')) {
            $query
                ->andWhere(($like ? $column . ' like :val' : $column . ' = :val'))
                ->setParameter('val', $like ? '%' . $filter . '%' : $filter);
        }
    }
}